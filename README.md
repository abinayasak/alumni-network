# Alumni Network Case
This is the backend API of the Alumni Network Case presented at Experis Academy Winter 2022.

## Table of contents
1. [Feature](#feature)
2. [Prerequisites](#prerequisites)
3. [Setup](#setup)
4. [Heroku](#heroku)
5. [Contributors](#contributors)

## Feature

GET METHOD:
- "http://localhost:8080/api/v1/user" gives a list of all the users in the database.
- "http://localhost:8080/api/v1/user/1" gives information of a single user with an id equal to one.
- "http://localhost:8080/api/v1/group" gives a list of all the groups in the database.
- "http://localhost:8080/api/v1/group/1" gives information a single group with an id equal to one
- "http://localhost:8080/api/v1/post/" gives a list of all the posts in the database.
- "http://localhost:8080/api/v1/post/Sara" gives a list of all the posts that was sent to the user Sara.
- "http://localhost:8080/api/v1/post/Sara/1" gives a list of all the posts that were sent to the user Sara from the user with an id equal to one.
- "http://localhost:8080/api/v1/post/group/1" gives a list of all the posts that were sent to a group with an id equal to one.
- "http://localhost:8080/api/v1/post/topic/3" gives a list of all the posts that were sent to a topic with an id equal to three.
- "http://localhost:8080/api/v1/post/event/1" gives a list of all the posts that were sent to an event with an id equal to one.
- "http://localhost:8080/api/v1/topic" gives a list of all the topics in the database.
- "http://localhost:8080/api/v1/topic/1" gives information of a single topic with an id equal to one.
- "http://localhost:8080/api/v1/event" gives a list of all the events in the database.
- "http://localhost:8080/api/v1/rsvp" gives a list of all the rsvp in the database.

PATCH METHOD:
- http://localhost:8080/api/v1/user/1 lets you update details of an already existing user with an id equal to one. 

POST METHOD:
- "http://localhost:8080/api/v1/event/1/invite/group/2" adds the group with an id equal to two to the event with an id equal to one.
- "http://localhost:8080/api/v1/event/1/invite/topic/1" adds the topic with an id equal to one to the event with an id equal to one.
- "http://localhost:8080/api/v1/event/1/invite/user/3" adds the user with an id equal to three to the event with an id equal to one.
- "http://localhost:8080/api/v1/event" lets you add a new event to the event list.
- "http://localhost:8080/api/v1/post" lets you add a new post to the post list.
- "http://localhost:8080/api/v1/group" lets you add a new group to the group list.
- "http://localhost:8080/api/v1/group/2/join" lets you add a user (id given in the body) to the group with an id equal to two.
- "http://localhost:8080/api/v1/topic/1/join" lets you add a user (id given in the body) to the topic with an id equal to one.
- "http://localhost:8080/api/v1/topic" lets you add a new topic to the topic list.
- "http://localhost:8080/api/v1/event/1/rsvp" adds a new event rsvp record.

PUT METHOD:
- "http://localhost:8080/api/v1/event/1" lets you update details of an already existing post with an id equal to one.
- "http://localhost:8080/api/v1/post/1" lets you update details of an already existing post with an id equal to one.

DELETE METHOD:
- "http://localhost:8080/api/v1/event/1/invite/group/1" deletes the group with an id equal to one from the event with an id equal to one.
- "http://localhost:8080/api/v1/event/1/invite/topic/1" deletes the topic with an id equal to one from the event with an id equal to one.
- "http://localhost:8080/api/v1/event/1/invite/user/2" deletes the user with an id equal to two from the event with an id equal to one. 


## Prerequisites
- Java 17
- IntelliJ IDEA
- pgAdmin 4
- Postman


## Setup
Open up the terminal and run the following code in the file you want the project to be saved in:
```java 
git clone git@gitlab.com:abinayasak/alumni-network.git
```
Open the cloned folder in IntelliJ IDEA and run the project by
clicking the run button in the top right corner, or by pressing the
control and the R button on your keyboard.


## Heroku 
- https://app-alumninetwork.herokuapp.com/


## Contributors
- Jo Urdal - @jourdal
- Abinaya Abbi Sakthivel @abinayasak
- Ole Henrik Johansen - @OleHJoh
- Daniel Mossestad - @Snaxai
- Robin Svanor - @robin.svanor