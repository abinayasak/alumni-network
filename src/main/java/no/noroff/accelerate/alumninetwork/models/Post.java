package no.noroff.accelerate.alumninetwork.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int postId;

    private LocalDateTime lastUpdated;
    private int senderId;
    private String postMessage;
    private String title;

    // Relationships:

    // One Post can have many Topics, and one Topic can have many Posts
    @ManyToMany
    @JoinTable(
            name = "topic_post",
            joinColumns = {@JoinColumn(name = "post_id")},
            inverseJoinColumns = {@JoinColumn(name = "topic_id")}
    )
    private Set<Topic> target_topics = new HashSet<>();

    // One event can have many Posts
    @ManyToOne
    private AlumniEvent target_event;

    // One group can have many Posts
    @ManyToOne
    private AlumniGroup target_group;

    // One Post(Target) can have many Posts
    @ManyToOne
    private Post postTarget;

    // One Alumnus can have many Posts
    @ManyToOne
    @JoinColumn(name="alumni_id")
    private Alumni target_user;

    @OneToMany()
    private List<Post> posts = new ArrayList<>();

    public Post(){}

    public Post(String title, String postMessage, LocalDateTime lastUpdated, Post postTarget, int senderId, Alumni target_user,
                AlumniGroup target_group, Set<Topic> target_topics, AlumniEvent target_event) {
        this.title = title;
        this.postMessage = postMessage;
        this.lastUpdated = lastUpdated;
        this.postTarget = postTarget;
        this.senderId = senderId;
        this.target_user = target_user;
        this.target_group = target_group;
        this.target_topics = target_topics;
        this.target_event = target_event;
    }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getSenderId() { return senderId; }

    public void setSenderId(int senderId) { this.senderId = senderId; }

    @JsonGetter("postTarget")
    public Integer getPostTarget() {
        if (postTarget != null){
            return postTarget.getPostId();
        }
        return null;
    }

    public void setPostTarget(Post postTarget) {
        this.postTarget = postTarget;
    }

    /*
    @JsonGetter("posts")
    public List<Integer> getPosts() {
        if (posts != null){
            List<Integer> id = new ArrayList<>();
            for (Post post : posts){
                id.add(post.getPostId());
            }
            return id;
        }
        return null;
    }*/

    public void setPosts(List<Post> posts) { this.posts = posts; }


    @JsonGetter("target_user")
    public Integer getTarget_user() {
        if (target_user != null){
            return target_user.getUserId();
        }
        return null;
    }

    public void setTarget_user(Alumni target_user) {
        this.target_user = target_user;
    }

    @JsonGetter("target_topics")
    public List<Integer> getTarget_topics() {
        if (target_topics != null){
            List<Integer> id = new ArrayList<>();
            for (Topic topic : target_topics){
                id.add(topic.getTopicId());
            }
            return id;
        }
        return null;
    }

    public void setTarget_topics(Set<Topic> target_topics) {
        this.target_topics = target_topics;
    }

    @JsonGetter("target_event")
    public Integer getTarget_event() {
        if (target_event != null){
            return target_event.getEventId();
        }
        return null;
    }

    public void setTarget_event(AlumniEvent target_event) {
        this.target_event = target_event;
    }

    @JsonGetter("target_group")
    public Integer getTarget_group() {
        if (target_group != null){
            return target_group.getGroupId();
        }
        return null;
    }

    public void setTarget_group(AlumniGroup target_group) {
        this.target_group = target_group;
    }

    public String getPostMessage() {
        return postMessage;
    }

    public void setPostMessage(String postMessage) {
        this.postMessage = postMessage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
