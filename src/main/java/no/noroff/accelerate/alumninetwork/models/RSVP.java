package no.noroff.accelerate.alumninetwork.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class RSVP {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int RSVPId;

    // Relationships:
    @ManyToOne
    @JoinColumn (name = "user_id")
    private Alumni alumni;

    @ManyToOne
    @JoinColumn (name = "event_id")
    private AlumniEvent event;

    private LocalDateTime lastUpdated;
    private int guestCount;

    public RSVP(){}

    public RSVP(Alumni alumni, AlumniEvent event, LocalDateTime lastUpdated, int guestCount) {
        this.alumni = alumni;
        this.event = event;
        this.lastUpdated = lastUpdated;
        this.guestCount = guestCount;
    }

    public int getRSVPId() {
        return RSVPId;
    }

    public void setRSVPId(int RSVPId) {
        this.RSVPId = RSVPId;
    }

    @JsonGetter("alumni")
    public int getAlumni() {
        return alumni.getUserId();
    }

    public void setAlumni(Alumni alumni) {
        this.alumni = alumni;
    }

    @JsonGetter("event")
    public int getEvent() {
        return event.getEventId();
    }

    public void setEvent(AlumniEvent event) {
        this.event = event;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(int guestCount) {
        this.guestCount = guestCount;
    }
}
