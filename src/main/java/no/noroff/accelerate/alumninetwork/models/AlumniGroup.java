package no.noroff.accelerate.alumninetwork.models;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class AlumniGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int groupId;
    private String name;
    private String description;
    private boolean isPrivate;

    // Relationships
    @ManyToMany
    @JoinTable(
            name = "group_member",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<Alumni> alumnus = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "event_group_invite",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "event_id")}
    )
    private Set<AlumniEvent> events = new HashSet<>();

    @OneToMany
    private List<Post> posts = new ArrayList<>();

    public AlumniGroup(){}

    public AlumniGroup(String name, String description, boolean isPrivate) {
        this.name = name;
        this.description = description;
        this.isPrivate = isPrivate;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    @JsonGetter("alumnus")
    public List<Integer> getAlumnus() {
        if (alumnus != null){
            return alumnus.stream().map(m -> m.getUserId()).collect(Collectors.toList());
        }
        return null;
    }

    public void setAlumnus(Set<Alumni> alumnus) {
        this.alumnus = alumnus;
    }

    @JsonGetter("events")
    public List<Integer> getEvents() {
        if (events != null){
            return events.stream().map(e -> e.getEventId()).collect(Collectors.toList());
        }
        return null;
    }

    public void setEvents(Set<AlumniEvent> events) {
        this.events = events;
    }

    @JsonGetter("posts")
    public List<Integer> getPosts() {
        if (posts != null){
            return posts.stream().map(p -> p.getPostId()).collect(Collectors.toList());
        }
        return null;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}