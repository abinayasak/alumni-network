package no.noroff.accelerate.alumninetwork.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Alumni {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;
    private String name;
    private String picture;
    private String status;
    private String bio;
    private String funFact;
    private String email;

    // Relationships:
    @ManyToMany
    @JoinTable(
            name = "group_member",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    private Set<AlumniGroup> groups = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "topic_member",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "topic_id")}
    )
    private Set<Topic> topics = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "event_user_invite",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "event_id")}
    )
    private Set<AlumniEvent> events = new HashSet<>();

    @OneToMany(mappedBy = "target_user")
    private List<Post> posts = new ArrayList<>();

    @OneToMany(mappedBy = "alumni")
    private Set<RSVP> rsvp = new HashSet<>();

    public Alumni() {
    }

    public Alumni(String name, String picture, String status, String bio, String funFact, String email) {
        this.name = name;
        this.picture = picture;
        this.status = status;
        this.bio = bio;
        this.funFact = funFact;
        this.email = email;
    }

    public Alumni(String name, String picture, String status, String bio, String funFact, String email, Set<AlumniEvent> events,
                  Set<AlumniGroup> groups, Set<Topic> topics) {
        this.name = name;
        this.picture = picture;
        this.status = status;
        this.bio = bio;
        this.funFact = funFact;
        this.email = email;
        this.events = events;
        this.groups = groups;
        this.topics = topics;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getFunFact() {
        return funFact;
    }

    public void setFunFact(String funFact) {
        this.funFact = funFact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonGetter("groups")
    public Set<Integer> getGroups() {
        return groups.stream().map(AlumniGroup::getGroupId).collect(Collectors.toSet());
    }

    public void setGroups(Set<AlumniGroup> groups) {
        this.groups = groups;
    }

    @JsonGetter("topics")
    public Set<Integer> getTopics() {
        return topics.stream().map(Topic::getTopicId).collect(Collectors.toSet());
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    @JsonGetter("events")
    public Set<Integer> getEvents() {
        return events.stream().map(AlumniEvent::getEventId).collect(Collectors.toSet());
    }

    public void setEvents(Set<AlumniEvent> events) {
        this.events = events;
    }

    @JsonGetter("posts")
    public List<Integer> getPosts() {
        return posts.stream().map(Post::getPostId).collect(Collectors.toList());
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    @JsonGetter("rsvps")
    public Set<Integer> getRSVP() {
        return rsvp.stream().map(RSVP::getRSVPId).collect(Collectors.toSet());
    }

    public void setRsvp(Set<RSVP> rsvp) {
        this.rsvp = rsvp;
    }
}
