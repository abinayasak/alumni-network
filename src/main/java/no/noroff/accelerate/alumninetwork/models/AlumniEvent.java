package no.noroff.accelerate.alumninetwork.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Entity
public class AlumniEvent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int eventId;
    private LocalDateTime lastUpdated;
    private int createdBy;
    private String title;
    private String description;
    private boolean allowGuests;
    private String bannerImg;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    // Relationships:
    @ManyToMany
    @JoinTable(
            name = "event_user_invite",
            joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<Alumni> users = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "event_group_invite",
            joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    private Set<AlumniGroup> groups = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "event_topic_invite",
            joinColumns = {@JoinColumn(name = "event_id")},
            inverseJoinColumns = {@JoinColumn(name = "topic_id")}
    )
    private Set<Topic> topics = new HashSet<>();

    @OneToMany(mappedBy = "event")
    private Set<RSVP> rsvps;

    @OneToMany
    private List<Post> posts = new ArrayList<>();


    public AlumniEvent() {
    }

    public AlumniEvent(LocalDateTime lastUpdated, int createdBy, String title, String description, boolean allowGuests, String bannerImg,
                       LocalDateTime startTime, LocalDateTime endTime, Set<Topic> topics, Set<AlumniGroup> groups) {
        this.lastUpdated = lastUpdated;
        this.createdBy = createdBy;
        this.title = title;
        this.description = description;
        this.allowGuests = allowGuests;
        this.bannerImg = bannerImg;
        this.startTime = startTime;
        this.endTime = endTime;
        this.topics = topics;
        this.groups = groups;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAllowGuests() {
        return allowGuests;
    }

    public void setAllowGuests(boolean allowGuests) {
        this.allowGuests = allowGuests;
    }

    public String getBannerImg() {
        return bannerImg;
    }

    public void setBannerImg(String bannerImg) {
        this.bannerImg = bannerImg;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @JsonGetter("users")
    public Set<Integer> getUsers() {
        return users.stream().map(Alumni::getUserId).collect(Collectors.toSet());
    }

    public void setUsers(Set<Alumni> users) {
        this.users = users;
    }

    @JsonGetter("groups")
    public List<Integer> getGroups() {
        if (groups != null){
            List<Integer> id = new ArrayList<>();
            for (AlumniGroup group : groups){
                id.add(group.getGroupId());
            }
            return id;
        }
        return null;
    }

    public void setGroups(Set<AlumniGroup> groups) {
        this.groups = groups;
    }

    @JsonGetter("topics")
    public List<Integer> getTopics() {
        if (topics != null){
            List<Integer> id = new ArrayList<>();
            for (Topic topic : topics){
                id.add(topic.getTopicId());
            }
            return id;
        }
        return null;
    }

    public void setTopics(Set<Topic> topics) {
        this.topics = topics;
    }

    @JsonGetter("rsvps")
    public Set<Integer> getRSVPs() {
        if (rsvps != null) {
            return rsvps.stream().map(RSVP::getRSVPId).collect(Collectors.toSet());
        }
        return null;
    }

    public void setRsvp(Set<RSVP> rsvp) {
        this.rsvps = rsvp;
    }

    @JsonGetter("posts")
    public List<Integer> getPosts() {
        return posts.stream().map(Post::getPostId).collect(Collectors.toList());
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<RSVP> getRsvps() {
        return rsvps;
    }

    public void setRsvps(Set<RSVP> rsvps) {
        this.rsvps = rsvps;
    }
}
