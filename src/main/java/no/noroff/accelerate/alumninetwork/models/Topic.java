package no.noroff.accelerate.alumninetwork.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int topicId;

    @NotNull
    @Column(name="name", nullable = false, length = 100)
    private String name;

    @Column(name="description", length = 200)
    private String description;

    // Relationships
    @ManyToMany
    @JoinTable(
            name = "topic_member",
            joinColumns = {@JoinColumn(name = "topic_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<Alumni> members = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "event_topic_invite",
            joinColumns = {@JoinColumn(name = "topic_id")},
            inverseJoinColumns = {@JoinColumn(name = "event_id")}
    )
    private Set<AlumniEvent> events = new HashSet<>();

    @ManyToMany
    private List<Post> posts = new ArrayList<>();

    public Topic(){}

    public Topic(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter("members")
    public List<Integer> getMembers() {
        if (members != null){
            return members.stream().map(m -> m.getUserId()).collect(Collectors.toList());
        }
        return null;
    }

    public void setMembers(Set<Alumni> alumnis) {
        this.members = alumnis;
    }

    @JsonGetter("events")
    public List<Integer> getEvents() {
        if (events != null){
            return events.stream().map(e -> e.getEventId()).collect(Collectors.toList());
        }
        return null;
    }

    public void setEvents(Set<AlumniEvent> events) {
        this.events = events;
    }

    @JsonGetter("posts")
    public List<Integer> getPosts() {
        if (posts != null){
            return posts.stream().map(p -> p.getPostId()).collect(Collectors.toList());
        }
        return null;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}