package no.noroff.accelerate.alumninetwork.repositories;

import no.noroff.accelerate.alumninetwork.models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Integer> {

}
