package no.noroff.accelerate.alumninetwork.repositories;

import no.noroff.accelerate.alumninetwork.models.AlumniGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlumniGroupRepository extends JpaRepository<AlumniGroup, Integer> {
}
