package no.noroff.accelerate.alumninetwork.repositories;

import no.noroff.accelerate.alumninetwork.models.AlumniEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlumniEventRepository extends JpaRepository<AlumniEvent, Integer> {
}
