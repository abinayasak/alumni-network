package no.noroff.accelerate.alumninetwork.repositories;

import no.noroff.accelerate.alumninetwork.models.Alumni;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlumniRepository extends JpaRepository<Alumni, Integer> {
}
