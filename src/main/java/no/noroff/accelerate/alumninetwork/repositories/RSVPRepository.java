package no.noroff.accelerate.alumninetwork.repositories;

import no.noroff.accelerate.alumninetwork.models.RSVP;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RSVPRepository extends JpaRepository<RSVP, Integer> {
}
