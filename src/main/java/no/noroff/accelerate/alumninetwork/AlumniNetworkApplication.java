package no.noroff.accelerate.alumninetwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlumniNetworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlumniNetworkApplication.class, args);
    }

}
