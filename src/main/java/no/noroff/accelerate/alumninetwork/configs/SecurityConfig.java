package no.noroff.accelerate.alumninetwork.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${auth0.audience}")
    private String audience;

    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuer;

    //Function to decode the jwtToken received from sender
    @Bean
    JwtDecoder jwtDecoder() {
        NimbusJwtDecoder jwtDecoder = (NimbusJwtDecoder)
                JwtDecoders.fromOidcIssuerLocation(issuer);

        //Checks if the jwt token contains the valid factors for access to the closed off api requests
        OAuth2TokenValidator<Jwt> audienceValidator = new AudienceValidator(audience);
        OAuth2TokenValidator<Jwt> withIssuer = JwtValidators.createDefaultWithIssuer(issuer);
        OAuth2TokenValidator<Jwt> withAudience = new DelegatingOAuth2TokenValidator<>(withIssuer, audienceValidator);

        jwtDecoder.setJwtValidator(withAudience);

        return jwtDecoder;
    }

    //Function for which api request need authorization and permits, and who are open to any
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .mvcMatchers("/api/public").permitAll()
                .mvcMatchers("/api/private").authenticated()
                .mvcMatchers("/api/private-scoped").hasAuthority("SCOPE_read:messages")
                .mvcMatchers("/api/v1/user").authenticated()
                .mvcMatchers("/api/v1/user/*").authenticated()
                .mvcMatchers("/api/v1/rsvp").authenticated()
                .mvcMatchers("/api/v1/group").authenticated()
                .mvcMatchers("/api/v1/group/*").authenticated()
                .mvcMatchers("/api/v1/group/*/*").authenticated()
                .mvcMatchers("/api/v1/event").authenticated()
                .mvcMatchers("/api/v1/event/*").authenticated()
                .mvcMatchers("/api/v1/event/*/*").authenticated()
                .mvcMatchers("/api/v1/event/*/*/*/*").authenticated()
                .mvcMatchers("/api/v1/post").authenticated()
                .mvcMatchers("/api/v1/post/*").authenticated()
                .mvcMatchers("/api/v1/post/*/*").authenticated()
                .mvcMatchers("/api/v1/topic").authenticated()
                .mvcMatchers("/api/v1/topic/*").authenticated()
                .mvcMatchers("/api/v1/topic/*/*").authenticated()
                .and().cors()
                .and().csrf().disable()
                .oauth2ResourceServer().jwt();
    }
}
