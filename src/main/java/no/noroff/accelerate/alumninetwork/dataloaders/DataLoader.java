package no.noroff.accelerate.alumninetwork.dataloaders;

import no.noroff.accelerate.alumninetwork.models.*;
import no.noroff.accelerate.alumninetwork.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Set;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    private AlumniRepository alumniRepository;
    @Autowired
    private AlumniGroupRepository alumniGroupRepository;
    @Autowired
    private AlumniEventRepository alumniEventRepository;
    @Autowired
    private TopicRepository topicRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private RSVPRepository rsvpRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        loadData();
    }

    private void loadData() {
        if (alumniRepository.count() == 0 && alumniGroupRepository.count() == 0 && alumniEventRepository.count() == 0) {

            final AlumniGroup group1 = alumniGroupRepository.save(new AlumniGroup("Accelerate Learning", "Noroff offers accelerated learning for work professionals", true));
            final AlumniGroup group2 = alumniGroupRepository.save(new AlumniGroup("Football", "If you love football, join this group!", false));
            final AlumniGroup group3 = alumniGroupRepository.save(new AlumniGroup("Oslo Party", "Want to join a random party in Oslo? Join this group!", true));
            final AlumniGroup group4 = alumniGroupRepository.save(new AlumniGroup("IT jobs in Oslo", "Looking for a job in Oslo? Join this group.", true));
            final AlumniGroup group5 = alumniGroupRepository.save(new AlumniGroup("World News", "Discuss world news and your view in this group.", false));
            final AlumniGroup group6 = alumniGroupRepository.save(new AlumniGroup("Restaurants to try", "Restaurant reviews and score", false));
            final AlumniGroup group7 = alumniGroupRepository.save(new AlumniGroup("League of Legends", "This group is for LoL gamers", true));
            final AlumniGroup group8 = alumniGroupRepository.save(new AlumniGroup("University", "Student party, student get-together, study-groups etc.", false));
            final AlumniGroup group9 = alumniGroupRepository.save(new AlumniGroup("Movies/TV shows", "Share your favorite movie and tv show.", false));
            final AlumniGroup group10 = alumniGroupRepository.save(new AlumniGroup("Pet lovers", "Share pictures of your pet!", true));


            final Topic topic1 = topicRepository.save(new Topic("Technology", ""));
            final Topic topic2 = topicRepository.save(new Topic("Economics", ""));
            final Topic topic3 = topicRepository.save(new Topic("Travel", ""));
            final Topic topic4 = topicRepository.save(new Topic("Fashion", ""));
            final Topic topic5 = topicRepository.save(new Topic("Work", ""));
            final Topic topic6 = topicRepository.save(new Topic("News", ""));
            final Topic topic7 = topicRepository.save(new Topic("Sports", ""));
            final Topic topic8 = topicRepository.save(new Topic("Family", ""));
            final Topic topic9 = topicRepository.save(new Topic("Music", ""));
            final Topic topic10 = topicRepository.save(new Topic("Movies", ""));
            final Topic topic11 = topicRepository.save(new Topic("Food", ""));
            final Topic topic12 = topicRepository.save(new Topic("Books", ""));
            final Topic topic13 = topicRepository.save(new Topic("TV Shows", ""));
            final Topic topic14 = topicRepository.save(new Topic("Pets", ""));
            final Topic topic15 = topicRepository.save(new Topic("Hobbies", ""));
            final Topic topic16 = topicRepository.save(new Topic("Restaurants", ""));
            final Topic topic17 = topicRepository.save(new Topic("Studies", ""));
            final Topic topic18 = topicRepository.save(new Topic("Gaming", ""));
            final Topic topic19 = topicRepository.save(new Topic("Podcasts", ""));
            final Topic topic20 = topicRepository.save(new Topic("Social Media", ""));
            final Topic topic21 = topicRepository.save(new Topic("Social", ""));
            final Topic topic22 = topicRepository.save(new Topic("Party", ""));


            LocalDateTime event1DateTimeStart = LocalDateTime.of(2022, Month.JUNE, 1, 18, 00, 00);
            LocalDateTime event1DateTimeEnd = LocalDateTime.of(2022, Month.JUNE, 1, 23, 00, 00);
            final AlumniEvent event1 = alumniEventRepository.save(new AlumniEvent(LocalDateTime.now(ZoneId.of("GMT+2")), 1, "Meetup", "Social gathering Noroff batch Winter 2022", true, null, event1DateTimeStart, event1DateTimeEnd, Set.of(topic1), Set.of(group1)));

            LocalDateTime event2DateTimeStart = LocalDateTime.of(2022, Month.DECEMBER, 31, 18, 00, 00);
            LocalDateTime event2DateTimeEnd = LocalDateTime.of(2023, Month.JANUARY, 1, 02, 00, 00);
            final AlumniEvent event2 = alumniEventRepository.save(new AlumniEvent(LocalDateTime.now(ZoneId.of("GMT+2")), 2, "New Years", "Happy New Year 2023", false, null, event2DateTimeStart, event2DateTimeEnd, Set.of(topic21, topic22), Set.of(group3)));

            LocalDateTime event3DateTimeStart = LocalDateTime.of(2022, Month.JULY, 17, 21, 00, 00);
            LocalDateTime event3DateTimeEnd = LocalDateTime.of(2023, Month.JULY, 18, 01, 00, 00);
            final AlumniEvent event3 = alumniEventRepository.save(new AlumniEvent(LocalDateTime.now(ZoneId.of("GMT+2")), 4, "Summer party", "Let's meet up in the park", true, null, event3DateTimeStart, event3DateTimeEnd, Set.of(topic21), Set.of(group1)));

            LocalDateTime event4DateTimeStart = LocalDateTime.of(2022, Month.MAY, 7, 17, 00, 00);
            LocalDateTime event4DateTimeEnd = LocalDateTime.of(2023, Month.MAY, 7, 23, 00, 00);
            final AlumniEvent event4 = alumniEventRepository.save(new AlumniEvent(LocalDateTime.now(ZoneId.of("GMT+2")), 7, "Birthday Party", "I'm getting old, let's celebrate!", true, null, event4DateTimeStart, event4DateTimeEnd, Set.of(topic21, topic22), Set.of(group8)));


            final Alumni user1 = alumniRepository.save(new Alumni("Cecilie", "https://thumbs.dreamstime.com/b/female-silhoutte-avatar-default-profile-picture-photo-placeholder-vector-illustration-130555145.jpg", "Fullstack Java Consultant at Experis Academy", "25 years old from Trondheim", "If I could have any superpower, it would be flying.", "mail1", Set.of(event1), Set.of(group1, group2, group6), Set.of(topic1,topic2,topic5)));
            final Alumni user2 = alumniRepository.save(new Alumni("Therese", null, "Fullstack Java Consultant at Experis Academy", "24 years old from Kristiansand", "If I could keep any animal as a pet, I would choose lamb.", "mail2", Set.of(event1, event2), Set.of(group4, group9, group10), Set.of(topic1, topic20)));
            final Alumni user3 = alumniRepository.save(new Alumni("Morten", null, "Fullstack Java Consultant at Experis Academy", "24 years old from Oslo", "If I get to choose my next life, I want to be a butterfly.", "urdaljo@gmail.com", Set.of(event3), Set.of(group3, group4), Set.of(topic1, topic4, topic19)));
            final Alumni user4 = alumniRepository.save(new Alumni("Mathias", "https://i.pinimg.com/originals/d9/56/9b/d9569bbed4393e2ceb1af7ba64fdf86a.jpg", "Java Developer at Netflix", "30 years old from Fredrikstad", "If I get to choose my next life, I want to be a dog.", "daniel.mossestad@gmail.com", Set.of(event4), Set.of(group9, group8), Set.of(topic1, topic5, topic18)));
            final Alumni user5 = alumniRepository.save(new Alumni("Jonas", null, "Senior Developer at Kongsberg Digital", "31 years old from Stavanger", "If I get to choose my next life, I want to be a bird.", "mail5", Set.of(event3), Set.of(group8, group5, group6), Set.of(topic1, topic6, topic17)));
            final Alumni user6 = alumniRepository.save(new Alumni("Thomas", null, "Front-end Developer at Entur", "26 years old from Oslo", "If I get to choose my next life, I want to be a butterfly.", "mail6", Set.of(event1, event2), Set.of(group7,group4), Set.of(topic1, topic7, topic16)));
            final Alumni user7 = alumniRepository.save(new Alumni("Truls", null, "Back-end Developer at SpareBank 1", "29 years old from Oslo", "If I could keep any animal as a pet, I would choose cow.", "mail7", Set.of(event1), Set.of(group6, group5, group8, group9), Set.of(topic1, topic8, topic15)));
            final Alumni user8 = alumniRepository.save(new Alumni("Sara", null, "Fullstack Developer at DNB", "22 years old from Oslo", "If I could keep any animal as a pet, I would choose cat.", "mail8", Set.of(event1,event2,event3, event4), Set.of(group5, group2, group7), Set.of(topic1, topic9, topic14)));
            final Alumni user9 = alumniRepository.save(new Alumni("Henriette", null, "Java Developer at Gjensidige", "24 years old from Oslo", "If I get to choose my next life, I want to be a bird.", "mail9", Set.of(), Set.of(group9,group2,group3), Set.of(topic1, topic10, topic13)));
            final Alumni user10 = alumniRepository.save(new Alumni("Maria", "https://ps.w.org/metronet-profile-picture/assets/icon-256x256.png?rev=2464419", "Python Developer at Netflix", "23 years old from Bergen", "If I could have any superpower, it would be invisible.", "mail10", Set.of(event3, event4), Set.of(group10, group1, group7), Set.of(topic1, topic11, topic12)));


            final RSVP rsvp1 = rsvpRepository.save(new RSVP(user1, event1, LocalDateTime.now(ZoneId.of("GMT+2")), 0));
            final RSVP rsvp2 = rsvpRepository.save(new RSVP(user2, event1, LocalDateTime.now(ZoneId.of("GMT+2")), 0));
            final RSVP rsvp3 = rsvpRepository.save(new RSVP(user3, event2, LocalDateTime.now(ZoneId.of("GMT+2")), 0));


            final Post post1 = postRepository.save(new Post("Noroff Gathering","Will you be joining the social gathering of Noroff?", LocalDateTime.now(ZoneId.of("GMT+2")), null, user2.getUserId(), user1, group1, Set.of(topic21), event1));
            final Post post2 = postRepository.save(new Post("Noroff Gathering","I will be there :)", LocalDateTime.now(ZoneId.of("GMT+2")), post1, user1.getUserId(), user2, group1, Set.of(topic21), event1));
            final Post post3 = postRepository.save(new Post("Noroff Gathering","Great!", LocalDateTime.now(ZoneId.of("GMT+2")), post1, user2.getUserId(), user1, group1, Set.of(topic21), event1));

            final Post post4 = postRepository.save(new Post("Case project","I'll be late to group-work.", LocalDateTime.now(ZoneId.of("GMT+2")), null, user3.getUserId(), user1, group1, Set.of(topic17), null));
            final Post post5 = postRepository.save(new Post("Case project","No problem. See you soon.", LocalDateTime.now(ZoneId.of("GMT+2")), post4, user1.getUserId(), user3, group1, Set.of(topic17), null));

            final Post post6 = postRepository.save(new Post("Safe to travel these days?","I really want to travel to Bali this summer, would it be safe to travel by then?", LocalDateTime.now(ZoneId.of("GMT+2")), null, user1.getUserId(), user2, null, Set.of(topic3), null));
            final Post post7 = postRepository.save(new Post("Safe to travel these days?", "I'm not too sure with the ongoing war. Maybe it's safer to wait.", LocalDateTime.now(ZoneId.of("GMT+2")), post6, user2.getUserId(), user1, null, Set.of(topic3), null));

            final Post post8 = postRepository.save(new Post("Summer Party","Should we bring anything?", LocalDateTime.now(ZoneId.of("GMT+2")), null, user10.getUserId(), null, null, Set.of(topic22), event3));
            final Post post9 = postRepository.save(new Post("Summer Party","Just drinks for yourself and maybe some hot dogs?", LocalDateTime.now(ZoneId.of("GMT+2")), post8, user8.getUserId(), user10, null, Set.of(topic22), event3));
            final Post post10 = postRepository.save(new Post("Summer Party","I'll bring hot dogs and ketchup.", LocalDateTime.now(ZoneId.of("GMT+2")), post8, user5.getUserId(), user8, null, Set.of(topic22), event3));
            final Post post11 = postRepository.save(new Post("Summer Party","Awesome! I'll bring hot dog bread then.", LocalDateTime.now(ZoneId.of("GMT+2")), post8, user10.getUserId(), user5, null, Set.of(topic22), event3));

            final Post post12 = postRepository.save(new Post("Work in OSLO","I'm looking for a fullstack Java developer in Oslo.", LocalDateTime.now(ZoneId.of("GMT+2")), null, user6.getUserId(), null, group4, Set.of(topic5), null));
            final Post post13 = postRepository.save(new Post("Work in OSLO","I'm looking for a NET. developer who is interested in remote work.", LocalDateTime.now(), null, user2.getUserId(), null, group4, Set.of(topic5), null));
            final Post post14 = postRepository.save(new Post("Work in OSLO","I'm looking for a backend Java developer in Molde.", LocalDateTime.now(ZoneId.of("GMT+2")), null, user3.getUserId(), null, group4, Set.of(topic5), null));

            final Post post15 = postRepository.save(new Post("TV Show to watch","What TV show would you guys recommend to watch?", LocalDateTime.now(ZoneId.of("GMT+2")), null, user9.getUserId(), null, group9, Set.of(topic13), null));
            final Post post16 = postRepository.save(new Post("TV Show to watch","Stranger Things! If you are into Sci-Fi.", LocalDateTime.now(ZoneId.of("GMT+2")), post15, user7.getUserId(), user9, group9, Set.of(topic13), null));
            final Post post17 = postRepository.save(new Post("TV Show to watch","Walking Dead, a classic one.", LocalDateTime.now(ZoneId.of("GMT+2")), post15, user4.getUserId(), user9, group9, Set.of(topic13), null));


        }
    }
}

