package no.noroff.accelerate.alumninetwork.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.noroff.accelerate.alumninetwork.models.Alumni;
import no.noroff.accelerate.alumninetwork.repositories.AlumniRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Temporary endpoints for user: get all users, get user profile by its id, and partial patch of user by its id
 */
@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/user")
public class AlumniController {

    @Autowired
    private AlumniRepository userRepository;

    @GetMapping("/login")
    public ResponseEntity<Alumni> getUser(@AuthenticationPrincipal Jwt principal) throws IOException {
        Alumni returnUser = new Alumni();

        String token = principal.getClaimAsString("http://alumni-network");
//        System.out.println(token);
        ObjectMapper mapper = new ObjectMapper();
        String email = String.valueOf(mapper.readTree(token).get("email"));
        String newEmail = email.replaceAll("^\"|\"$", "");
//        System.out.println(newEmail);

        List<Alumni> alumnus = userRepository.findAll();
        for (int i = 0; i < alumnus.size(); i++) {
//            System.out.println(alumnus.get(i).getEmail());
            if (alumnus.get(i).getEmail().equals(newEmail)) {
                returnUser = alumnus.get(i);
            }
        }

        HttpStatus status = HttpStatus.SEE_OTHER;

        return new ResponseEntity<>(returnUser, status);
    }

    @GetMapping()
    public ResponseEntity<List<Alumni>> getAllUsers(){
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(userRepository.findAll(), status);
    }

    /**
     * Get user by its id
     * @param user_token
     * @return
     */
    @GetMapping("/{user_id}")
    public ResponseEntity<Alumni> getUserProfile(@AuthenticationPrincipal Jwt user_token) {
        Alumni returnUser = new Alumni();
        HttpStatus status;

        // Checks if the user exists
        if (userRepository.existsById(Integer.valueOf(user_token.getClaimAsString("id")))) {
            status = HttpStatus.OK;
            returnUser = userRepository.findById(Integer.valueOf(user_token.getClaimAsString("id"))).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(returnUser, status);
    }

    /**
     * Partial patch of user by its id
     * @param user_id
     * @param user
     * @return
     */
    @PatchMapping("/{user_id}")
    public ResponseEntity<Alumni> updateUser(@PathVariable Integer user_id, @RequestBody Alumni user) {
        Alumni returnUser = userRepository.findById(user_id).get();

        if (user.getName() != null) {
            returnUser.setName(user.getName());
        }
        if (user.getPicture() != null) {
            returnUser.setPicture(user.getPicture());
        }
        if (user.getStatus() != null) {
            returnUser.setStatus(user.getStatus());
        }
        if (user.getBio() != null) {
            returnUser.setBio(user.getBio());
        }
        if (user.getFunFact() != null) {
            returnUser.setFunFact(user.getFunFact());
        }

        return new ResponseEntity<>(userRepository.save(returnUser), HttpStatus.NO_CONTENT);
    }

    /**
     *
     * @param principal
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Alumni> createUser(@AuthenticationPrincipal Jwt principal) throws IOException {
        // Function uses the auth0 token to get information to create the user the first time they register to the website
        String token = principal.getClaimAsString("http://alumni-network");
//        System.out.println(token);
        ObjectMapper mapper = new ObjectMapper();
        String email = String.valueOf(mapper.readTree(token).get("email"));
        String picture = String.valueOf(mapper.readTree(token).get("picture"));
        String name = String.valueOf(mapper.readTree(token).get("name"));
        String newEmail = email.replaceAll("^\"|\"$", "");
        String newPicture = picture.replaceAll("^\"|\"$", "");
        String newName = name.replaceAll("^\"|\"$", "");


        Alumni newUser = new Alumni();
        newUser.setName(newName);
        newUser.setPicture(newPicture);
        newUser.setEmail(newEmail);
        newUser.setFunFact("A Fun Fact");
        newUser.setBio("This is my bio");
        newUser.setStatus("This is my status");
        return new ResponseEntity<>(userRepository.save(newUser), HttpStatus.CREATED);
    }
}
