package no.noroff.accelerate.alumninetwork.controllers;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.alumninetwork.models.*;
import no.noroff.accelerate.alumninetwork.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Temporary endpoints for event: get all events, create a new event, (completely) update an event by its id,
 * create event group invitation, create event topic invitation, create event user invitation,
 * delete event group invitation, delete event topic invitation, delete event user invitation,
 * and create rsvp record
 */
@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/event")
public class EventController {

    @Autowired
    private AlumniEventRepository eventRepository;
    @Autowired
    private AlumniGroupRepository groupRepository;
    @Autowired
    private TopicRepository topicRepository;
    @Autowired
    private AlumniRepository userRepository;
    @Autowired
    private RSVPRepository rsvpRepository;

    /**
     * Gets a list of all events (temporary) - should only get events posted to groups and topics the user is subscribed to
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
    })
    @GetMapping
    public ResponseEntity<List<AlumniEvent>> getEvents() {
        return new ResponseEntity<>(eventRepository.findAll(), HttpStatus.OK);
    }


    /**
     * Creates a new event (temporary) - should check if the user is part of the audience where the event is posted &
     * set the creator
     * @param event
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @PostMapping
    public ResponseEntity<AlumniEvent> createEvent(@RequestBody AlumniEvent event) {
        event.setLastUpdated(LocalDateTime.now(ZoneId.of("GMT+2")));
        // Set the creator
        return new ResponseEntity<>(eventRepository.save(event), HttpStatus.CREATED);
    }


    /**
     * Updates an event completely (temporary) - should check if the user is the OP of the event
     * @param event
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Successful request. There is no content to send for this request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request. The server cannot or will not process the request due to something that is perceived to be a client error.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @PutMapping("/{event_id}")
    public ResponseEntity<AlumniEvent> updateEvent(@PathVariable Integer event_id, @RequestBody AlumniEvent event) {
        AlumniEvent returnEvent = eventRepository.findById(event_id).get();
        HttpStatus status;

        // Checks if the id matches
        if (!event_id.equals(event.getEventId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnEvent, status);
        }

        if (!eventRepository.findById(event_id).get().getTopics().containsAll(event.getTopics()) ||
                !eventRepository.findById(event_id).get().getGroups().containsAll(event.getGroups())
        ){
            status = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(returnEvent, status);
        }

        // Makes sure linked data related to users, groups and topics is kept
        Set<Integer> integerUsers = returnEvent.getUsers();
        List<Alumni> users = userRepository.findAllById(integerUsers);
        Set<Alumni> userSet = new HashSet<>(users);
        event.setUsers(userSet);

        List<Integer> integerGroups = returnEvent.getGroups();
        List<AlumniGroup> groupList = groupRepository.findAllById(integerGroups);
        Set<AlumniGroup> groupSet = new HashSet<>(groupList);
        event.setGroups(groupSet);

        List<Integer> integerTopics = returnEvent.getTopics();
        List<Topic> topicList = topicRepository.findAllById(integerTopics);
        Set<Topic> topicSet = new HashSet<>(topicList);
        event.setTopics(topicSet);

        // Sets lastUpdated to now and updates the event
        event.setLastUpdated(LocalDateTime.now(ZoneId.of("GMT+2")));
        returnEvent = eventRepository.save(event);
        status = HttpStatus.NO_CONTENT;

        return new ResponseEntity<>(returnEvent, status);
    }


    /**
     * Creates an event group invitation for the event (temporary) - should check if the user is the OP of the event
     * @param event_id
     * @param group_id
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @PostMapping("/{event_id}/invite/group/{group_id}")
    public ResponseEntity<AlumniEvent> createEventGroupInvite(@PathVariable Integer event_id, @PathVariable Integer group_id) {
        AlumniEvent event = eventRepository.getById(event_id);
        Set<AlumniGroup> groups = new HashSet<>(groupRepository.findAllById(event.getGroups()));
        AlumniGroup group = groupRepository.getById(group_id);
        groups.add(group);

        event.setGroups(groups);
        return new ResponseEntity<>(eventRepository.save(event), HttpStatus.CREATED);
    }


    /**
     * Deletes an event group invitation for the event (temporary) - should check if the user is the OP of the event
     * @param event_id
     * @param group_id
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @DeleteMapping("/{event_id}/invite/group/{group_id}")
    public ResponseEntity<AlumniEvent> deleteEventGroupInvite(@PathVariable Integer event_id, @PathVariable Integer group_id) {
        AlumniEvent event = eventRepository.getById(event_id);
        Set<AlumniGroup> groups = new HashSet<>(groupRepository.findAllById(event.getGroups()));
        AlumniGroup group = groupRepository.getById(group_id);
        groups.remove(group);

        event.setGroups(groups);
        return new ResponseEntity<>(eventRepository.save(event), HttpStatus.OK);
    }


    /**
     * Creates an event topic invitation for the event (temporary) - should check if the user is the OP of the event
     * @param event_id
     * @param topic_id
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @PostMapping("/{event_id}/invite/topic/{topic_id}")
    public ResponseEntity<AlumniEvent> createEventTopicInvite(@PathVariable Integer event_id, @PathVariable Integer topic_id) {
        AlumniEvent event = eventRepository.getById(event_id);
        Set<Topic> topics = new HashSet<>(topicRepository.findAllById(event.getTopics()));
        Topic topic = topicRepository.getById(topic_id);
        topics.add(topic);

        event.setTopics(topics);
        return new ResponseEntity<>(eventRepository.save(event), HttpStatus.CREATED);
    }


    /**
     * Deletes an event topic invitation for the event (temporary) - should check if the user is the OP of the event
     * @param event_id
     * @param topic_id
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @DeleteMapping("/{event_id}/invite/topic/{topic_id}")
    public ResponseEntity<AlumniEvent> deleteEventTopicInvite(@PathVariable Integer event_id, @PathVariable Integer topic_id) {
        AlumniEvent event = eventRepository.getById(event_id);
        Set<Topic> topics = new HashSet<>(topicRepository.findAllById(event.getTopics()));
        Topic topic = topicRepository.getById(topic_id);
        topics.remove(topic);

        event.setTopics(topics);
        return new ResponseEntity<>(eventRepository.save(event), HttpStatus.OK);
    }


    /**
     * Creates an event user invitation for the event (temporary) - should check if the user is the OP of the event
     * @param event_id
     * @param user_id
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @PostMapping("/{event_id}/invite/user/{user_id}")
    public ResponseEntity<AlumniEvent> createEventUserInvite(@PathVariable Integer event_id, @PathVariable Integer user_id) {
        AlumniEvent event = eventRepository.getById(event_id);
        Set<Alumni> users = new HashSet<>(userRepository.findAllById(event.getUsers()));
        Alumni user = userRepository.getById(user_id);
        users.add(user);

        event.setUsers(users);
        return new ResponseEntity<>(eventRepository.save(event), HttpStatus.CREATED);
    }


    /**
     * Deletes an event user invitation for the event (temporary) - should check if the user is the OP of the event
     * @param event_id
     * @param user_id
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @DeleteMapping("/{event_id}/invite/user/{user_id}")
    public ResponseEntity<AlumniEvent> deleteEventUserInvite(@PathVariable Integer event_id, @PathVariable Integer user_id) {
        AlumniEvent event = eventRepository.getById(event_id);
        Set<Alumni> users = new HashSet<>(userRepository.findAllById(event.getUsers()));
        Alumni user = userRepository.getById(user_id);
        users.remove(user);

        event.setUsers(users);
        return new ResponseEntity<>(eventRepository.save(event), HttpStatus.OK);
    }


    /**
     * Create a rsvp record for an event (WIP) - need user_id from requesting user
     * @param event_id
     * @param rsvp
     * @return
     */
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @PostMapping("/{event_id}/rsvp")
    public ResponseEntity<RSVP> createRSVPRecord(@PathVariable Integer event_id, @RequestBody RSVP rsvp) {
        AlumniEvent event = eventRepository.getById(event_id);
        Set<RSVP> rsvps = new HashSet<>(rsvpRepository.findAllById(event.getRSVPs()));
        // Get rsvp from requesting user

        event.setLastUpdated(LocalDateTime.now(ZoneId.of("GMT+2")));
        return new ResponseEntity<>(rsvpRepository.save(rsvp), HttpStatus.CREATED);
    }
}
