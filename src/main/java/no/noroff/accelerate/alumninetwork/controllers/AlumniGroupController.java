package no.noroff.accelerate.alumninetwork.controllers;


import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.alumninetwork.models.*;
import no.noroff.accelerate.alumninetwork.repositories.AlumniGroupRepository;
import no.noroff.accelerate.alumninetwork.repositories.AlumniRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/group")
public class AlumniGroupController {

    @Autowired
    private AlumniGroupRepository alumniGroupRepository;

    @Autowired
    private AlumniRepository alumniRepository;


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniGroup.class)) }),
    })
    @GetMapping
    // Get all groups in the database
    // GET: http://localhost:8080/api/v1/group
    public ResponseEntity<List<AlumniGroup>> getAllGroups(){
        List<AlumniGroup> returnGroup = new ArrayList<>();
        HttpStatus status;

        /*
        Alumni user = new Alumni();
        List<AlumniGroup> groups = alumniGroupRepository.findAll();
        for (AlumniGroup group : groups){
            if (group.isPrivate() && !group.getAlumnus().contains(user.getUserId())){
            } else {
                returnGroup.add(group);
            }
        }*/

        status = HttpStatus.OK;
        returnGroup = alumniGroupRepository.findAll();
        return new ResponseEntity<>(returnGroup, status);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniGroup.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("{group_id}")
    // Get one group, specified with id, in the database
    // GET: http://localhost:8080/api/v1/group/1
    public ResponseEntity<AlumniGroup> getGroup(@PathVariable int group_id){
        AlumniGroup returnGroup = new AlumniGroup();
        HttpStatus status;

        if (!alumniGroupRepository.existsById(group_id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnGroup, status);
        }

        status = HttpStatus.OK;
        returnGroup = alumniGroupRepository.findById(group_id).get();
        return new ResponseEntity<>(returnGroup, status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniGroup.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Bad Request. The server cannot or will not process the request due to something that is perceived to be a client error.",
                    content = @Content),
    })
    @PostMapping()
    // Create a new group in the database
    // POST: http://localhost:8080/api/v1/group
    public ResponseEntity<AlumniGroup> createGroup(@RequestBody AlumniGroup group){
        AlumniGroup returnGroup = new AlumniGroup();
        HttpStatus status;

        if (group.getName().isEmpty()){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnGroup, status);
        }

        status = HttpStatus.CREATED;
        returnGroup = alumniGroupRepository.save(group);
        return new ResponseEntity<>(returnGroup, status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniGroup.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
    })
    @PostMapping("{group_id}/join")
    // Create a new group membership record in the database
    // POST: http://localhost:8080/api/v1/group/1/join
    public ResponseEntity<AlumniGroup> createGroupMembership(@RequestBody int user_id, @PathVariable int group_id){
        AlumniGroup returnGroup = new AlumniGroup();
        HttpStatus status;

        if (!alumniGroupRepository.existsById(group_id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnGroup, status);
        }

        if (!alumniRepository.existsById(user_id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnGroup, status);
        }

        AlumniGroup group = alumniGroupRepository.getById(group_id);
        Set<Alumni> users = new HashSet<>(alumniRepository.findAllById(group.getAlumnus()));

        if (group.isPrivate() && !users.contains(user_id)){
            status = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(returnGroup, status);
        }

        Alumni user = alumniRepository.getById(user_id);
        users.add(user);
        group.setAlumnus(users);

        status = HttpStatus.CREATED;
        returnGroup = alumniGroupRepository.save(group);
        return new ResponseEntity<>(returnGroup, status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK. Successfully deleted.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
    })
    @DeleteMapping("{group_id}/leave")
    public ResponseEntity<AlumniGroup> deleteGroupMembership(@RequestBody int user_id, @PathVariable int group_id) {
        AlumniGroup group = alumniGroupRepository.getById(group_id);
        Set<Alumni> users = new HashSet<>(alumniRepository.findAllById(group.getAlumnus()));
        Alumni user = alumniRepository.getById(user_id);
        users.remove(user);
        group.setAlumnus(users);
        return new ResponseEntity<>(alumniGroupRepository.save(group), HttpStatus.OK);
    }
}
