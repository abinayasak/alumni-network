package no.noroff.accelerate.alumninetwork.controllers;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.alumninetwork.models.RSVP;
import no.noroff.accelerate.alumninetwork.models.Topic;
import no.noroff.accelerate.alumninetwork.repositories.RSVPRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/rsvp")
public class RSVPController {

    private RSVPRepository rsvpRepository;

    public RSVPController(RSVPRepository rsvpRepository) {
        this.rsvpRepository = rsvpRepository;
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RSVP.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
    })
    @GetMapping("")
    public ResponseEntity<List<RSVP>> getAllRSVP() {
        return new ResponseEntity<>(rsvpRepository.findAll(), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RSVP.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
    })
    /**
     * Gets all rsvps connected to an event
     * @param event_id
     * @return
     */
    @GetMapping("{event_id}")
    public ResponseEntity<List<RSVP>> getAllRSVPForAnEvent(@PathVariable Integer event_id) {
        HttpStatus status = HttpStatus.OK;
        if (!rsvpRepository.existsById(event_id)) {
            status = HttpStatus.NOT_FOUND;
        }

        List<RSVP> rsvps = rsvpRepository.findAll();
        List<RSVP> rsvpWithEvent = new ArrayList<>();

        for (final RSVP rsvp : rsvps) {
            if (rsvp.getEvent() == event_id) {
                rsvpWithEvent.add(rsvp);
            }
        }

        return new ResponseEntity<>(rsvpWithEvent, status);
    }

    /**
     * @param rsvp
     * @return
     */
    @PostMapping("")
    public ResponseEntity<RSVP> createRSVP(@RequestBody RSVP rsvp) {
        return new ResponseEntity<>(rsvpRepository.save(rsvp), HttpStatus.CREATED);
    }
}
