package no.noroff.accelerate.alumninetwork.controllers;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.alumninetwork.models.*;
import no.noroff.accelerate.alumninetwork.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/post")
public class PostController {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private AlumniRepository alumniRepository;

    @Autowired
    private AlumniGroupRepository alumniGroupRepository;

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private AlumniEventRepository alumniEventRepository;


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
    })
    @GetMapping()
    // Returns a list of posts to groups and topics for which the requesting user is subscribed
    // GET: http://localhost:8080/api/v1/post
    public ResponseEntity<List<Post>> getAllPosts(@AuthenticationPrincipal Jwt user_token){
        List<Post> returnPost = new ArrayList<>();
        HttpStatus status;

        int userId = Integer.valueOf(user_token.getClaimAsString("id"));
        Alumni user = alumniRepository.getById(userId);

        List<Post> posts = postRepository.findAll();
        for (final Post post : posts) {
            if (user.getGroups().contains(post.getTarget_group()) ||
                    user.getEvents().contains(post.getTarget_event()) ||
                    user.getTopics().contains(post.getTarget_topics()) ||
                    user.getUserId() == post.getSenderId()
            ){
                returnPost.add(post);
            }
        }

        returnPost = posts;
        status = HttpStatus.OK;
        Collections.reverse(returnPost);
        return new ResponseEntity<>(returnPost, status);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("/user/{id}")
    // Returns a list of posts from groups, events and topics the user is subscribed to.
    // GET: http://localhost:8080/api/v1/post/1
    public ResponseEntity<List<Post>> getPostById(@PathVariable int id){
        List<Post> returnPost = new ArrayList<>();
        HttpStatus status;

        Alumni user = alumniRepository.getById(id);

        List<Post> posts = postRepository.findAll();
        for (final Post post : posts) {
            if (user.getGroups().contains(post.getTarget_group()) ||
                    user.getEvents().contains(post.getTarget_event()) ||
                    user.getTopics().contains(post.getTarget_topics()) ||
                    user.getUserId() == post.getSenderId()
            ){
                returnPost.add(post);
            }
        }
        status = HttpStatus.OK;
        Collections.reverse(returnPost);
        return new ResponseEntity<>(returnPost, status);
    }



    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("{user}")
    // Returns a list of posts that were sent as direct messages to the requesting user
    // GET: http://localhost:8080/api/v1/post/sara
    public ResponseEntity<List<Post>> getAllPostToUser(@PathVariable String user){
        List<Post> returnPost = new ArrayList<>();
        HttpStatus status;

        List<Alumni> users = alumniRepository.findAll();
        Alumni findUser = new Alumni();
        for (final Alumni u : users) {
            if (u.getName().equals(user)){
                findUser = u;
            }
        }

        List<Post> posts = postRepository.findAll();
        for (final Post post : posts) {
            if (post.getTarget_user() != null){
                if (post.getTarget_user() == findUser.getUserId()){
                    returnPost.add(post);
                }
            }
        }
        status = HttpStatus.OK;
        Collections.reverse(returnPost);
        return new ResponseEntity<>(returnPost, status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("{user}/{user_id}")
    // Returns a list of posts that were sent as direct messages to the requesting user from the specific user described by user_id
    // GET: http://localhost:8080/api/v1/post/sara/1
    public ResponseEntity<List<Post>> getAllPostToUserFromUserID(@PathVariable String user, @PathVariable int user_id){
        List<Post> returnPost = new ArrayList<>();
        HttpStatus status;

        if (!alumniRepository.existsById(user_id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnPost, status);
        }

        List<Alumni> users = alumniRepository.findAll();
        Alumni findUser = new Alumni();
        for (final Alumni u : users) {
            if (u.getName().equals(user)){
                findUser = u;
            }
        }

        List<Post> posts = postRepository.findAll();
        for (final Post post : posts) {
            if (post.getTarget_user() != null){
                if (post.getTarget_user() == findUser.getUserId() && post.getSenderId() == user_id){
                    returnPost.add(post);
                }
            }
        }

        status = HttpStatus.OK;
        Collections.reverse(returnPost);
        return new ResponseEntity<>(returnPost, status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("group/{group_id}")
    // Returns a list of posts that were sent with the group described by group_id as the target audience
    // GET: http://localhost:8080/api/v1/post/group/1
    public ResponseEntity<List<Post>> getPostsInGroup(@PathVariable int group_id){
        List<Post> returnPost = new ArrayList<>();
        HttpStatus status;

        if (!alumniGroupRepository.existsById(group_id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnPost, status);
        }

        List<Post> posts = postRepository.findAll();
        for (final Post post : posts) {
            if (post.getTarget_group() != null){
                if (post.getTarget_group() == group_id){
                    returnPost.add(post);
                }
            }
        }
        status = HttpStatus.OK;
        Collections.reverse(returnPost);
        return new ResponseEntity<>(returnPost, status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("topic/{topic_id}")
    // Returns a list of posts that were sent with the topic described by topic_id as the target audience
    // GET: http://localhost:8080/api/v1/post/topic/1
    public ResponseEntity<List<Post>> getPostsInTopic(@PathVariable int topic_id){
        List<Post> returnPost = new ArrayList<>();
        HttpStatus status;

        if (!topicRepository.existsById(topic_id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnPost, status);
        }

        List<Post> posts = postRepository.findAll();
        for (final Post post : posts) {
            if (post.getTarget_topics() != null){
                for (final Integer id : post.getTarget_topics()){
                    if (id == topic_id){
                        returnPost.add(post);
                    }
                }
            }
        }

        status = HttpStatus.OK;
        Collections.reverse(returnPost);
        return new ResponseEntity<>(returnPost, status);
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("event/{event_id}")
    // Returns a list of posts that were sent with the event described by event_id as the target audience
    // GET: http://localhost:8080/api/v1/post/event/1
    public ResponseEntity<List<Post>> getPostsInEvent(@PathVariable int event_id){
        List<Post> returnPost = new ArrayList<>();
        HttpStatus status;

        if (!alumniEventRepository.existsById(event_id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnPost, status);
        }

        List<Post> posts = postRepository.findAll();
        for (final Post post : posts) {
            if (post.getTarget_event() != null){
                if (post.getTarget_event() == event_id){
                    returnPost.add(post);
                }
            }
        }
        status = HttpStatus.OK;
        Collections.reverse(returnPost);
        return new ResponseEntity<>(returnPost, status);

    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request. The server cannot or will not process the request due to something that is perceived to be a client error.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content)
    })
    @PostMapping()
    // Create a new post
    // POST: http://localhost:8080/api/v1/post
    public ResponseEntity<Post> createPost(@RequestBody Post post){
        Post returnPost = new Post();
        HttpStatus status;

        if (post.getPostMessage().isEmpty() ||
                post.getTitle().isEmpty() ||
                post.getSenderId() == 0
        ){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnPost, status);
        }

        /*
        After authentication is fixed

        if (!alumniRepository.findById(user_id).get().getGroups().contains(post.getTarget_group()) ||
                !alumniRepository.findById(user_id).get().getTopics().contains(post.getTarget_topics()) ||
                !alumniRepository.findById(user_id).get().getEvents().contains(post.getTarget_event())
        ){
            status = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(returnPost, status);
        }
        */

        post.setLastUpdated(LocalDateTime.now(ZoneId.of("GMT+2")));
        returnPost = postRepository.save(post);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnPost, status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Post.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request. The server cannot or will not process the request due to something that is perceived to be a client error.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized Access. The request authentication failed or the user does not have permission for the requested operation.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden. The client does not have access rights to the content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @PutMapping("{post_id}")
    // Update an existing post
    // PUT: http://localhost:8080/api/v1/post/1
    public ResponseEntity<Post> updatePost(@PathVariable int post_id, @RequestBody Post post){
        Post returnPost = new Post();
        HttpStatus status;

        if (post_id != post.getPostId()){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnPost, status);
        }
        if (!postRepository.existsById(post_id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(returnPost, status);
        }


        if (!postRepository.findById(post_id).get().getTarget_topics().containsAll(post.getTarget_topics()) ||
                postRepository.findById(post_id).get().getTarget_event() != post.getTarget_event() ||
                postRepository.findById(post_id).get().getTarget_group() != post.getTarget_group()
        ){
            status = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(returnPost, status);
        }

        post.setLastUpdated(LocalDateTime.now(ZoneId.of("GMT+2")));
        returnPost = postRepository.save(post);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnPost, status);
    }


}
