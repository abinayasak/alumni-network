package no.noroff.accelerate.alumninetwork.controllers;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.accelerate.alumninetwork.models.*;
import no.noroff.accelerate.alumninetwork.repositories.AlumniRepository;
import no.noroff.accelerate.alumninetwork.repositories.TopicRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/topic")
public class TopicController {

    private TopicRepository topicRepository;
    private AlumniRepository userRepository;

    public TopicController(TopicRepository topicRepository, AlumniRepository userRepository) {
        this.topicRepository = topicRepository;
        this.userRepository = userRepository;
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Topic.class)) }),
    })
    @GetMapping("")
    public ResponseEntity<List<Topic>> getAllTopics() {
        return new ResponseEntity<>(topicRepository.findAll(), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful request.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Topic.class)) }),
            @ApiResponse(responseCode = "404", description = "Data Not Found. The server can not find the requested resource.",
                    content = @Content),
    })
    @GetMapping("{topic_id}")
    // Get one topic, specified with id, in the database
    // GET: http://localhost:8080/api/v1/topic/1
    public ResponseEntity<Topic> getTopic(@PathVariable int topic_id) {
        HttpStatus status = HttpStatus.OK;
        if (!topicRepository.existsById(topic_id)) {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(topicRepository.findById(topic_id).get(), status);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Topic.class)) }),
    })
    @PostMapping("")
    // Create a new topic in the database
    // POST: http://localhost:8080/api/v1/topic
    public ResponseEntity<Topic> postTopic(@RequestBody Topic topic) {
        return new ResponseEntity<>(topicRepository.save(topic), HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful request and data were created.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Topic.class)) }),
    })
    @PostMapping("{topic_id}/join")
    // Create a new topic membership record in the database
    // POST: http://localhost:8080/api/v1/topic/1/join
    public ResponseEntity<Topic> createTopicMembership(@RequestBody Integer user_id, @PathVariable Integer topic_id) {
        Topic topic = topicRepository.getById(topic_id);
        Set<Alumni> users = new HashSet<>(userRepository.findAllById(topic.getMembers()));
        Alumni user = userRepository.getById(user_id);
        users.add(user);
        topic.setMembers(users);
        return new ResponseEntity<>(topicRepository.save(topic), HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK. Successfully deleted.",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AlumniEvent.class)) }),
    })
    @DeleteMapping("{topic_id}/remove")
    // Removes the user from topic membership
    // DELETE: http://localhost:8080/api/v1/topic/1/remove
    public ResponseEntity<Topic> deleteTopicMembership(@RequestBody Integer user_id, @PathVariable Integer topic_id) {
        Topic topic = topicRepository.getById(topic_id);
        Set<Alumni> users = new HashSet<>(userRepository.findAllById(topic.getMembers()));
        Alumni user = userRepository.getById(user_id);
        users.remove(user);
        topic.setMembers(users);
        return new ResponseEntity<>(topicRepository.save(topic), HttpStatus.OK);
    }
}