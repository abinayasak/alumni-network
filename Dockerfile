# Base gradle image used to build jar
FROM gradle:jdk17-alpine AS gradle
# Set directory in image to hold our project
WORKDIR /app
# Copy everything into that directory
COPY . .
# Builds the jar for the project
# bootJar builds a jar just for execution - no testing
# Testing is done as a seperate stage in a pipeline
RUN gradle bootJar

FROM openjdk:17 as runtime
# Use the same directory as before
WORKDIR /app
# Explicitly set port for deployments to platforms like Heroku
ENV PORT 8080
# Set profile for production
ENV SPRING_PROFILE production
# Additional environment variables
ENV DATABASE_URL ""
#ENV ISSUER_URL ""
#ENV JWKS_URI ""
#ENV CLIENT_ID "client-id"
#ENV CLIENT_SECRET ""
ENV DDL_AUTO "create"
#ENV APP_ORIGIN "http://localhost:3000"
# Where the jar file is built
ARG JAR_FILE=/app/build/libs/*.jar
# Takes our jar file from app/build/libs/jarname.jar and moves it to /app/app.jar
COPY --from=gradle ${JAR_FILE} /app/app.jar
# Change ownership of folder to stop running as root
RUN chown -R 1000:1000 /app
USER 1000:1000
# What command is run when a container is created for this image
# Dserver.port: https://stackoverflow.com/a/21083284
ENTRYPOINT ["java","-jar", \
    "-Dserver.port=${PORT}", \
    "-Dspring.profiles.active=${SPRING_PROFILE}", \
    "-Dspring.datasource.url=jdbc:${DATABASE_URL}", \
    "-Dspring.jpa.hibernate.ddl-auto=${DDL_AUTO}", \
#    "-Dspring.security.oauth2.resourceserver.jwt.issuer-uri=${ISSUER_URL}", \
#    "-Dspring.security.oauth2.resourceserver.jwt.jwk-set-uri=${JWKS_URI}", \
#    "-Dspring.swagger-ui.oauth.client-id=${CLIENT_ID}", \
#    "-Dspring.swagger-ui.oauth.client-secret=${CLIENT_SECRET}", \
#    "-Dapp.cors.application_origin=${APP_ORIGIN}", \
    "app.jar"]